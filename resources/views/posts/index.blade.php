@extends('layout.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            <form action="{{ route('posts') }}" method="post">
                @csrf
                <div class="mb-4">
                    <textarea rows="5" name="body" id="body" placeholder="Post Something!!!" class="w-full bg-gray-100 p-2 border-2 rounded-lg @error('body') border-red-500 @enderror"></textarea>
                    @error('body')
                        <p class="text-sm text-red-500 mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium justify-right">Post Now</button>
                </div>
            </form>
            <div class="mt-2">
                @if ($posts->count())
                    @foreach ($posts as $post)
                        <div class="mb-6 mt-6">
                            <a class="font-bold text-xl">{{ $post->user->username }} </a>
                            <span class="text-gray-500 text-sm">{{ $post->created_at->diffForHumans() }}</span>
                            
                            <div class="text-lg mb-2 mt-2">{{ $post->body }}</div>

                            @can('delete', $post)
                                <div class="flex items-center">
                                    <form action="{{ route('posts.delete', $post) }}" method="post" class="">
                                        @csrf
                                        @method('DELETE')
                                        <button class="text-red-500">Delete post</button>
                                    </form>
                                </div>
                            @endcan
                        
                            <div class="flex items-center">
                                @auth
                                    @if (!$post->likedBy(auth()->user()))
                                        <form action="{{ route('posts.like', $post) }}" method="post" class="mr-1">
                                            @csrf
                                            <button class="text-blue-500">Like</button>
                                        </form>
                                    @else
                                        <form action="{{ route('posts.unlike', $post) }}" method="post" class="mr-1">
                                            @csrf
                                            @method('DELETE')
                                            <button class="text-blue-500">Unlike</button>
                                        </form>
                                    @endif
                                    <p class="">{{ $post->likes->count() }} {{ Str::plural('like', $post->likes->count()) }}</p>
                                @endauth
                            </div>  
                        </div><hr>
                    @endforeach
                    {{ $posts->links() }}
                @else
                    <p class="text-lg font-bold text-red-600">No post available!!!</p>
                @endif
            </div>
            
        </div> 
    </div>
@endsection