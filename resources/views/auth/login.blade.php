@extends('layout.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-5/12 bg-white p-6 rounded-lg">
            <p class="flex justify-center text-4xl">Register</p>
            @if (session('status'))
                <div class="bg-red-500 text-white text-center px-4 py-2 mt-2 mb-2 rounded w-full">{{ session('status') }}</div>
            @endif
            <form action="{{ route("login") }}" method="POST">
                @csrf
                <div class="mb-4">
                    <label for="email"> Email</label>
                    <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="Your Email" class="w-full bg-gray-100 p-2 border-2 rounded-lg @error('email') border-red-500 @enderror">
                    @error('email')
                        <p class="text-sm text-red-500 mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" placeholder="Your Password" class="w-full bg-gray-100 p-2 border-2 rounded-lg @error('password') border-red-500 @enderror">
                    @error('password')
                        <p class="text-sm text-red-500 mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-4">
                    <input type="checkbox" name="remember" id="remember" class="mr-2">
                    <label for="remember">Remember me</label>
                </div>

                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium w-full">Login</button>
                </div>
            </form>
        </div>
    </div>
@endsection