@extends('layout.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-5/12 bg-white p-6 rounded-lg">
            <p class="flex justify-center text-4xl">Register</p>
            <form action="{{ route("register") }}" method="POST">
                @csrf
                <div class="mb-4">
                    <label for="name">Your Name</label>
                    <input type="text" name="name" id="name" value="{{ old('name') }}" placeholder="Your Name" class="w-full bg-gray-100 p-2 border-2 rounded-lg @error('name') border-red-500 @enderror">
                    @error('name')
                        <p class="text-sm text-red-500 mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="username">User Name</label>
                    <input type="text" name="username" id="username" value="{{ old('username') }}" placeholder="User Name" class="w-full bg-gray-100 p-2 border-2 rounded-lg @error('username') border-red-500 @enderror">
                    @error('username')
                        <p class="text-sm text-red-500 mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="email"> Email</label>
                    <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="Your Email" class="w-full bg-gray-100 p-2 border-2 rounded-lg @error('email') border-red-500 @enderror">
                    @error('email')
                        <p class="text-sm text-red-500 mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" placeholder="Your Password" class="w-full bg-gray-100 p-2 border-2 rounded-lg @error('password') border-red-500 @enderror">
                    @error('password')
                        <p class="text-sm text-red-500 mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="password_confirmation">Confirm Password</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" class="w-full bg-gray-100 p-2 border-2 rounded-lg">
                </div>

                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium w-full">Register</button>
                </div>
            </form>
        </div>
    </div>
@endsection