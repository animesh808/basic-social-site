<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['only' => ['like','unlike']]);
    }
    public function index(){
        $data = Post::with(['user','likes'])->orderBy('created_at', 'desc')->paginate(15);
        $posts = [
            "posts" => $data
        ];
        
        return view('posts.index', $posts);
    }

    public function create(Request $request){
        $this->validate($request,[
            'body' => 'required'
        ]);
        
        $request->user()->post()->create($request->only('body'));

        return back();
    }

    public function destroy(Post $post){
        $this->authorize('delete', $post);
        $post->delete();
        return back();
    }

    public function like(Post $post, Request $request){

        if($post->likedBy($request->user())){
            return response('Already liked', 409);
        }
        $post->likes()->create([
            'user_id' => $request->user()->id,
        ]);
        return back();
    }

    public function unlike(Post $post, Request $request){
       $request->user()->likes()->where('post_id', $post->id)->delete();
       return back();
    }
}
